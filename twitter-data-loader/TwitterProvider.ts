import * as log from 'loglevel'
import to from 'await-to-js'
import * as Twitter from 'twitter-lite'

import { Politician } from './objects/Politician'
import { Tweet } from './objects/Tweet'
import { ITwitterList } from './interfaces'
import { flatten } from './utils'

log.setLevel(log.levels.INFO, false)

export class TwitterProvider {
    private client: Twitter

    public async authenticate() {
        // authenticate using Bearer Auth
        const user = new Twitter({
            consumer_key: process.env.TWITTER_API_KEY,
            consumer_secret: process.env.TWITTER_API_SECRET
        })

        const response = await user.getBearerToken()
        this.client = new Twitter({
            bearer_token: response.access_token
        })
    }

    private async loadPoliticiansFrom(owner: string, listName: string, currentPoliticians: Politician[]) {
        const [err, result] = await to(this.client.get('lists/members', {
            slug: listName,
            owner_screen_name: owner,
            count: 1000,
            include_entities: false,
            skip_status: true
        }))

        if (err) {
            log.error(err)
            return null
        }
        
        const talkingHeads: Politician[] = []
        result.users.forEach(user => {
            const existing = currentPoliticians.length === 0 ? null : currentPoliticians.find(head => head.twitterId === user.id_str)
            if (existing !== null) {
                talkingHeads.push(existing)
            } else {
                talkingHeads.push({
                    twitterId: user.id_str,
                    createdAt: new Date(user.created_at),
                    displayName: user.name,
                    followersCount: user.followers_count,
                    location: user.location,
                    profileImageUrl: user.profile_image_url_https,
                    tweetCount: user.statuses_count,
                    twitterHandle: user.screen_name,
                    latestTweetId: ''
                })
            }
        })
        return talkingHeads
    }

    public async getAllPoliticians(listsToFetch: ITwitterList[], currentPoliticians: Politician[]) {
        let allHeads: Politician[]

        // fetch all politicians according to supplied lists, flatten into big list
        await Promise.all(
            listsToFetch.map(list => this.loadPoliticiansFrom(list.owner, list.listName, currentPoliticians))
        ).then(nested => allHeads = flatten(nested))

        // remove duplicates
        const deDupedHeads = allHeads.filter((head, i) => {
            return i === allHeads.findIndex(obj => {
                return obj.twitterId === head.twitterId
            })
        })

        return deDupedHeads
    }

    public async getLatestTweetsForPolitician(screenName: string, sinceId: string = '') : Promise<Tweet[]>
    {
        const query: any = {
            screen_name: screenName,
            count: 40, // we have to always fetch too many tweets becuase retweets aren't respected
            trim_user: true,
            exclude_replies: true,
            include_rts: false,
            tweet_mode: 'extended'
        }

        if (sinceId !== '') { query.since_id = sinceId }

        const [err, result] = await to(this.client.get('statuses/user_timeline', query))        
        if (err) {
            log.error(err)
            return []
        }

        const allegedFacts: Tweet[] = []
        if (!result) { return allegedFacts }
        try
        {
            result.slice(0, 14).forEach(tweet => {
                allegedFacts.push({
                    id: tweet.id_str,
                    userId: tweet.user.id_str,
                    userScreenName: screenName,
                    createdAt: new Date(tweet.created_at),
                    text: tweet.full_text,
                    hashTags: tweet.entities.hashtags !== null ? tweet.entities.hashtags.map(ht => ht.text) : [],
                    retweetCount: tweet.retweet_count,
                    possiblySensitive: tweet.possibly_sensitive,
                    lang: tweet.lang,
                })
            })
        } catch (err) {
            if (result.error === 'Not authorized.') {
                log.info(`Someone doesn't want to play ball, got "Not authorized." error from twitter for ${screenName}.`)
            }
        }

        return allegedFacts
    }
}
