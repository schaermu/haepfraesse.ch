import 'source-map-support/register'

import * as log from 'loglevel'
import format from 'date-fns/format'
import sampleSize = require('lodash/sampleSize')
import sortBy = require('lodash/sortBy')
import last = require('lodash/last')

import { TwitterProvider } from './TwitterProvider'
import { FileStorageProvider } from './FileStorageProvider'
import { Politician } from './objects/Politician'

log.setLevel(log.levels.INFO, false)

export async function handler(event: any, context: any) {
    log.info('Loading latest tweets for all registered politicians...')

    const storage = new FileStorageProvider(process.env.S3_BUCKET_DATA)
    const twitterClient = new TwitterProvider()    
    await twitterClient.authenticate()

    const politicians = await storage.get<Politician[]>('politicians.json')

    let fetchPromises = politicians.map(head => twitterClient.getLatestTweetsForPolitician(head.twitterHandle, head.latestTweetId))
    // for local debugging purposes, we don't fetch all politicians. Instead, we fetch a random sample of 10 on each call
    if (process.env.IS_OFFLINE) {
        fetchPromises = sampleSize(politicians, 10).map(head => twitterClient.getLatestTweetsForPolitician(head.twitterHandle, head.latestTweetId))
    }

    // fetch & persist all tweets
    await Promise.all(fetchPromises).then(tweetsPerHead => {
        // store the id of the latest tweet into our politicians structure
        for (const tweets of tweetsPerHead) {
            if (!tweets || tweets.length === 0) { break }
            //const latestTweet = tweets.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime())[0]
            const latestTweet = last(sortBy(tweets, ['createdAt']))
            log.debug(latestTweet)
            const headIndex = politicians.findIndex(head => head.twitterId === latestTweet.userId)
            if (headIndex === -1) { break }
            politicians[headIndex].latestTweetId = latestTweet.id
        }

        return Promise.all([
            // persist updated master file with latest tweet id
            storage.save('politicians.json', Buffer.from(JSON.stringify(politicians), 'utf8')),            
            // persist all tweets to s3 using the twitter handle as a prefix
            tweetsPerHead.filter(tweets => tweets.length > 0).map(tweets => storage.save(
                `talkinghead-${tweets[0].userScreenName}/tweets-${format(new Date(), 'ddMMyyyy-HHmm')}.json`,
                Buffer.from(JSON.stringify(tweets), 'utf8')
            )
        )])
    })

    return
}
