import 'source-map-support/register'

import * as log from 'loglevel'
import { TwitterProvider } from './TwitterProvider'
import { FileStorageProvider } from './FileStorageProvider'
import { Politician } from './objects/Politician'
import to from 'await-to-js'

log.setLevel(log.levels.INFO, false)

export async function handler(event: any, context: any) {
    log.info('Loading latest list of politicians...')
    const twitterClient = new TwitterProvider()
    await twitterClient.authenticate()

    const storage = new FileStorageProvider(process.env.S3_BUCKET_DATA)

    const [error, currentPoliticians] = await to(storage.get<Politician[]>('politicians.json'))

    const politicians = await twitterClient.getAllPoliticians(
        [
            { owner: 'kubleag', listName: 'schweizer-politiker' },
            { owner: 'swissinfo_de', listName: 'schweizer-politiker' }
        ], error ? new Array<Politician>() : currentPoliticians)

    // persist back to S3
    await storage.save('politicians.json', Buffer.from(JSON.stringify(politicians), 'utf8'))
    return
}
