export class Tweet {
    public id: string
    public userScreenName: string
    public userId: string
    public createdAt: Date
    public text: string
    public hashTags: string[]
    public retweetCount: number
    public possiblySensitive: boolean
    public lang: string
}
