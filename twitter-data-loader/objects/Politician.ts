export class Politician {
    public twitterId: string
    public twitterHandle: string
    public displayName: string
    public location: string
    public followersCount: BigInt
    public createdAt: Date
    public tweetCount: number
    public profileImageUrl: string
    public latestTweetId: string
}
