import AWS = require('aws-sdk')
import * as log from 'loglevel'

log.setLevel(log.levels.INFO, false)

export class FileStorageProvider {
    private bucket: string
    private s3Client: AWS.S3

    constructor(bucket: string) {
        this.bucket = bucket

        const s3Config: AWS.S3.ClientConfiguration = {
            signatureVersion: 'v4',
            endpoint: process.env.IS_OFFLINE ? 'http://localhost:8000' : 'https://s3.eu-central-1.amazonaws.com',
            s3ForcePathStyle: true
        }

        if (s3Config.endpoint.indexOf('localhost') > -1) {
            // tslint:disable-next-line: deprecation
            s3Config.accessKeyId = s3Config.secretAccessKey = 'S3RVER'
        }

        this.s3Client = new AWS.S3(s3Config)
    }

    public async save(fileName: string, body: Buffer): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.s3Client.putObject({
                Bucket: this.bucket,
                Key: fileName,
                Body: body
            },
            (err) => {
                if (err) {
                    log.error(err)
                    return reject(err)
                }
                return resolve(true)
            })
        })
    }

    public async get<T>(fileName: string): Promise<T> {
        return new Promise((resolve, reject) => {
            this.s3Client.getObject({
                Bucket: this.bucket,
                Key: fileName,
            },
            (err, result) => {
                if (err) {
                    log.error(err)
                    return reject(err)
                }
                return resolve(JSON.parse(result.Body.toString()) as T)
            })
        })
    }
}
