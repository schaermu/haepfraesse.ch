export function flatten (arr: any[]) {
    return [].concat.apply([], arr)
}
